package tutorial.mvc;

public class Calculator {

    public int abs(int arg)
    {
        return arg < 0 ? -arg : arg;
    }
}
