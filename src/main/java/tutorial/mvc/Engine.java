package tutorial.mvc;

public interface Engine {
    void increaseRpm();
    int getRpm();
}
