package tutorial.core.services;

import tutorial.core.entities.BlogEntry;

public interface BlogEntryService {
    BlogEntry find(Long id);
}
