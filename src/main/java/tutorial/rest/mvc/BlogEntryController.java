package tutorial.rest.mvc;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tutorial.core.entities.BlogEntry;
import tutorial.core.services.BlogEntryService;
import tutorial.resources.BlogEntryResource;
import tutorial.resources.asm.BlogEntryResourceAsm;

import javax.xml.ws.Response;

@Controller
public class BlogEntryController {

    @RequestMapping("/blogtest1")
    public ResponseEntity<Object> test1() {
        BlogEntry entry = new BlogEntry();
        entry.setTitle("Test Blog Entry");

        return new ResponseEntity<Object>(entry, HttpStatus.OK);
    }

    @RequestMapping("/blogtest2")
    public @ResponseBody BlogEntry test2() {
        BlogEntry entry = new BlogEntry();
        entry.setTitle("Test Blog Entry");

        return entry;
    }

    @RequestMapping(value="/blogtest3", method = RequestMethod.POST)
    public @ResponseBody BlogEntry postTest(@RequestBody BlogEntry entry) {
        return entry;
    }

    private BlogEntryService service;

    public BlogEntryController(BlogEntryService service) {
        this.service = service;
    }

    @RequestMapping(value="/rest/blog-entries/{blogEntryId}", method=RequestMethod.GET)
    public ResponseEntity<BlogEntryResource> getBlogEntry(@PathVariable Long blogEntryId) {
        BlogEntry entry = service.find(blogEntryId);

        if (entry != null) {
            BlogEntryResource res = new BlogEntryResourceAsm().toResource(entry);
            return new ResponseEntity<BlogEntryResource>(res, HttpStatus.OK);
        } else  {
            return new ResponseEntity<BlogEntryResource>(HttpStatus.NOT_FOUND);
        }

    }
}
