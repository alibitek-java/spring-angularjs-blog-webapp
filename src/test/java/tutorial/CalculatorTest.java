package tutorial;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tutorial.mvc.Calculator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CalculatorTest {

    @Mock
    private Calculator calc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAbs() {
        Calculator calc = new Calculator();
        assertEquals(4, calc.abs(-4));

        when(calc.abs(-20)).thenReturn(20);
        assertEquals(20, calc.abs(-20));
    }

}
